class Prediction{
    public required string Label { get; set; }
    public float Confidence { get; set; }
}