﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ML.OnnxRuntime.Tensors;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Microsoft.ML.OnnxRuntime.ResNet50v2Sample
{
    class Program
    {
        public static void Main(string[] args)
        {
            var imageFilePaths = new[] { "dog1.jpg", "dog2.jpg", "dog3.jpg", "dog4.jpg" };

            var images = imageFilePaths.Select(NormalizeImage).ToArray();

            // To generate the input can refer the github page, always has readme to generate input for the model
            foreach (var image in images)
            {
                using (image)
                {
                    PredictImage(image);
                }
            }
        }

        private static void PredictImage(Image<Rgb24> image)
        {
            // Preprocess image
            // first dimension is batch size
            // second dimension is channels (RGB)
            // third and fourth dimensions are height and width
            var input = new DenseTensor<float>(new[] { 1, 3, 224, 224 });

            var mean = new[] { 0.485f, 0.456f, 0.406f };
            var stddev = new[] { 0.229f, 0.224f, 0.225f };
            image.ProcessPixelRows(accessor =>
                {
                    for (int y = 0; y < accessor.Height; y++)
                    {
                        Span<Rgb24> pixelSpan = accessor.GetRowSpan(y);
                        for (int x = 0; x < accessor.Width; x++)
                        {
                            input[0, 0, y, x] = NormalizeColor(pixelSpan[x].R, mean[0], stddev[0]);
                            input[0, 1, y, x] = NormalizeColor(pixelSpan[x].G, mean[1], stddev[1]);
                            input[0, 2, y, x] = NormalizeColor(pixelSpan[x].B, mean[2], stddev[2]);
                        }
                    }
                });


            // Need to use netron(https://netron.app/) to check the model's input & output
            // Then setup the input & output accordingly
            // Setup inputs
            var inputs = new List<NamedOnnxValue>
            {
                NamedOnnxValue.CreateFromTensor("data", input)
            };

            // Run inference
            string modelFilePath = "resnet50-v2-7.onnx";
            using var session = new InferenceSession(modelFilePath);
            using IDisposableReadOnlyCollection<DisposableNamedOnnxValue> results = session.Run(inputs);

            // Postprocess to get softmax vector
            IEnumerable<float> output = results.First().AsEnumerable<float>();
            float sum = output.Sum(x => (float)Math.Exp(x));
            IEnumerable<float> softmax = output.Select(x => (float)Math.Exp(x) / sum);

            // Extract top 10 predicted classes
            IEnumerable<Prediction> top10 = softmax.Select((x, i) => new Prediction { Label = LabelMap.Labels[i], Confidence = x })
                               .OrderByDescending(x => x.Confidence)
                               .Take(10);

            // Print results to console
            Console.WriteLine("Top 10 predictions for ResNet50 v2...");
            Console.WriteLine("--------------------------------------------------------------");
            foreach (var t in top10)
            {
                Console.WriteLine($"Label: {t.Label}, Confidence: {t.Confidence}");
            }
        }

        private static Image<Rgb24> NormalizeImage(string imageFilePath)
        {
            var image = Image.Load<Rgb24>(imageFilePath);

            // Resize image
            image.Mutate(x =>
            {
                x.Resize(new ResizeOptions
                {
                    Size = new Size(224, 224),
                    Mode = ResizeMode.Crop
                });
            });

            return image;
        }

        private static float NormalizeColor(byte color, float colorMean, float colorStdDev)
        {
            return ((color / 255f) - colorMean) / colorStdDev;
        }
    }
}